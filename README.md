# apache-hive

Finding the top 5 airlines with the greatest average DEPARTURE_DELAY using Apache Hive.

## Requirements

* [Docker 24+](https://www.docker.com/get-started)
* Docker-compose 2.21+

## Bootstraping your environment
```sh
$ docker-compose up -d
# wait while environment initialization is complete
```
## Run
```sh
$ ./run.sh
```

